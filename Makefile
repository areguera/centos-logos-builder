###############################################################################
# Update files related to centos-logos package
# Copyright (C) 2021-2022 Alain Reguera Delgado
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU General Public License as published by the Free Software
# Foundation, either version 3 of the License, or (at your option) any later
# version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program.  If not, see <https://www.gnu.org/licenses/>.
###############################################################################

#==============================================================================
# Identity resources
#==============================================================================
BRANDS := ../../../../Brands
MOTIFS := ../../../../Motifs
TYPOGRAPHIES := ../../../../Typographies
export JEKYLL_SRC_DIR := ../../../Websites/jekyll-theme-centos/src
JEKYLL_WEBFONTS_FONTAWESOME := $(wildcard $(TYPOGRAPHIES)/Fontawesome/webfonts/*.woff2)
JEKYLL_WEBFONTS_MONTSERRAT :=	$(wildcard $(TYPOGRAPHIES)/Montserrat/fonts/webfonts/*.woff2)
JEKYLL_WEBFONTS_OVERPASSMONO := $(wildcard $(TYPOGRAPHIES)/Overpass/fonts/webfonts_mono/*.woff2)
JEKYLL_WEBFONTS := $(JEKYLL_WEBFONTS_FONTAWESOME) $(JEKYLL_WEBFONTS_MONTSERRAT) $(JEKYLL_WEBFONTS_OVERPASSMONO)

#==============================================================================
# Commands
#==============================================================================
MOGRIFY := /usr/bin/mogrify
CONVERT := /usr/bin/convert
MOGRIFY_ARGS = -remap $(BRANDS)/Palettes/centos-corporate-colors.png +dither
INKSCAPE := /usr/bin/inkscape
PNGQUANT := /usr/bin/pngquant -f --ext .png
TESTPAGE := /usr/bin/python testpage/testpage.py
MKDIR := /usr/bin/mkdir -p
WKHTMLTOIMAGE := /usr/bin/wkhtmltoimage --quiet --width 1920
TIDY := /usr/bin/tidy -config testpage/tidy.conf
LN := /usr/bin/ln -fs
BASE64 := /usr/bin/base64 -w0

#==============================================================================
# Dependencies
#==============================================================================
testpage-img-dir := testpage/img
testpage-img := $(testpage-img-dir) \
	$(testpage-img-dir)/centos-motif.png \
	$(testpage-img-dir)/centos-motif.png.base64 \
	$(testpage-img-dir)/centos-logo.svg \
	$(testpage-img-dir)/centos-logo.svg.base64 \
	$(testpage-img-dir)/centos-symbol.svg \
	$(testpage-img-dir)/centos-symbol.svg.base64 \
	$(testpage-img-dir)/centos-whitelogo.svg \
	$(testpage-img-dir)/centos-whitelogo.svg.base64 \
	$(testpage-img-dir)/favicon.png \
	$(testpage-img-dir)/favicon.png.base64
testpage-webfonts-dir := testpage/webfonts
testpage-css-dir:= testpage/css
testpage-css := $(testpage-css-dir) \
	$(testpage-css-dir)/stylesheet.min.css

#==============================================================================
# Macros
#==============================================================================
define fonts-base64
	@echo Creating webfonts in base64 format...;
	for FONT in $(JEKYLL_WEBFONTS); do $(BASE64) $${FONT} > $(testpage-webfonts-dir)/$$(basename $${FONT}.base64); done
endef

#==============================================================================
# Final distribution
#==============================================================================
dst_dir := ../../Packages/centos-logos
dst_files = \
	$(dst_dir)/testpage/index.html \
	$(dst_dir)/anaconda/theme/sidebar-logo.png \
	$(dst_dir)/anaconda/theme/sidebar-bg.png \
	$(dst_dir)/anaconda/theme/topbar-bg.png \
	$(dst_dir)/icons/hicolor/scalable/apps/start-here.svg \
	$(dst_dir)/icons/hicolor/scalable/apps/fedora-logo-icon.svg \
	$(dst_dir)/icons/hicolor/scalable/apps/xfce4_xicon1.svg \
	$(dst_dir)/icons/hicolor/scalable/apps/org.fedoraproject.AnacondaInstaller.svg \
	$(dst_dir)/icons/hicolor/symbolic/apps/org.fedoraproject.AnacondaInstaller-symbolic.svg \
	$(dst_dir)/icons/hicolor/16x16/apps/fedora-logo-icon.png \
	$(dst_dir)/icons/hicolor/16x16/apps/system-logo-icon.png \
	$(dst_dir)/icons/hicolor/22x22/apps/fedora-logo-icon.png \
	$(dst_dir)/icons/hicolor/22x22/apps/system-logo-icon.png \
	$(dst_dir)/icons/hicolor/24x24/apps/fedora-logo-icon.png \
	$(dst_dir)/icons/hicolor/24x24/apps/system-logo-icon.png \
	$(dst_dir)/icons/hicolor/32x32/apps/fedora-logo-icon.png \
	$(dst_dir)/icons/hicolor/32x32/apps/system-logo-icon.png \
	$(dst_dir)/icons/hicolor/36x36/apps/fedora-logo-icon.png \
	$(dst_dir)/icons/hicolor/36x36/apps/system-logo-icon.png \
	$(dst_dir)/icons/hicolor/48x48/apps/fedora-logo-icon.png \
	$(dst_dir)/icons/hicolor/48x48/apps/system-logo-icon.png \
	$(dst_dir)/icons/hicolor/48x48/apps/anaconda.png \
	$(dst_dir)/icons/hicolor/96x96/apps/fedora-logo-icon.png \
	$(dst_dir)/icons/hicolor/96x96/apps/system-logo-icon.png \
	$(dst_dir)/icons/hicolor/256x256/apps/fedora-logo-icon.png \
	$(dst_dir)/icons/hicolor/256x256/apps/system-logo-icon.png \
	$(dst_dir)/ipa/header-logo.png \
	$(dst_dir)/ipa/product-name.png \
	$(dst_dir)/ipa/login-screen-logo.png \
	$(dst_dir)/pixmaps/fedora-logo-sprite.svg \
	$(dst_dir)/pixmaps/fedora-gdm-logo.png \
	$(dst_dir)/pixmaps/fedora-logo.png \
	$(dst_dir)/pixmaps/fedora-logo-small.png \
	$(dst_dir)/pixmaps/fedora-logo-sprite.png \
	$(dst_dir)/pixmaps/poweredby.png \
	$(dst_dir)/pixmaps/system-logo-white.png \
	$(dst_dir)/fedora/fedora_logo.svg \
	$(dst_dir)/fedora/fedora_logo_lightbackground.svg \
	$(dst_dir)/fedora/fedora_logo_darkbackground.svg

#==============================================================================
# Default rule
#==============================================================================
all: $(dst_files) testpage/screenshot.png

#===============================================================================
# Testpage
#===============================================================================
$(dst_dir)/testpage/index.html: $(testpage-img) $(testpage-webfonts-dir) $(testpage-css) testpage/index.html.j2
	$(TESTPAGE) testpage/index.html.j2 $@
	$(TIDY) -o $@ $@

$(testpage-img-dir) $(testpage-css-dir):
	$(MKDIR) $@

$(testpage-webfonts-dir):
	$(MKDIR) $@
	$(fonts-base64)

$(testpage-img-dir)/centos-motif.png: $(MOTIFS)/Final/centos-default-0-2048x1536.jpg
	$(CONVERT) $< -resize 1024x768 $@
	$(PNGQUANT) $@

$(testpage-img-dir)/centos-motif.png.base64: $(testpage-img-dir)/centos-motif.png
	$(BASE64) $< > $@

$(testpage-img-dir)/centos-logo.svg: $(BRANDS)/Sources/centos-logo.svg
	$(INKSCAPE) --vacuum-defs --export-plain-svg --export-filename=$@ $<

$(testpage-img-dir)/centos-logo.svg.base64: $(testpage-img-dir)/centos-logo.svg
	$(BASE64) $< > $@

$(testpage-img-dir)/centos-symbol.svg: $(BRANDS)/Sources/centos-symbol.svg
	$(INKSCAPE) --vacuum-defs --export-plain-svg --export-filename=$@ $<

$(testpage-img-dir)/centos-symbol.svg.base64: $(testpage-img-dir)/centos-symbol.svg
	$(BASE64) $< > $@

$(testpage-img-dir)/centos-whitelogo.svg: $(BRANDS)/Sources/centos-whitelogo.svg
	$(INKSCAPE) --vacuum-defs --export-plain-svg --export-filename=$@ $<

$(testpage-img-dir)/centos-whitelogo.svg.base64: $(testpage-img-dir)/centos-whitelogo.svg
	$(BASE64) $< > $@

$(testpage-img-dir)/favicon.png: $(BRANDS)/Sources/centos-symbol.svg
	$(INKSCAPE) --batch-process --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) -remap $(BRANDS)/Palettes/centos-corporate-colors.png +dither -resize x48 $@
	$(PNGQUANT) $@

$(testpage-img-dir)/favicon.png.base64: $(testpage-img-dir)/favicon.png
	$(BASE64) $< > $@

$(testpage-css-dir)/stylesheet.min.css: $(JEKYLL_SRC_DIR)/_site/assets/css/stylesheet.min.css
	$(LN) ../../$< $@

testpage/screenshot.png:
	-$(WKHTMLTOIMAGE) $< $@
	$(PNGQUANT) $@

#===============================================================================
# Anaconda
#===============================================================================
$(dst_dir)/anaconda/theme/sidebar-logo.png: $(BRANDS)/Sources/centos-gdm-whitelogo.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x32 $@
	$(PNGQUANT) $@

$(dst_dir)/anaconda/theme/sidebar-bg.png: $(dst_dir)/backgrounds/centos-default-0-1536x2048.jpg
	$(CONVERT) -resize x1200 -crop 240x1200+330+0 $< $@
	$(PNGQUANT) $@

$(dst_dir)/anaconda/theme/topbar-bg.png: $(dst_dir)/backgrounds/centos-default-0-2048x1536.jpg
	$(CONVERT) -resize 1920x -crop 1920x132+0+0 $< $@
	$(PNGQUANT) $@

#===============================================================================
# IPA
#===============================================================================
$(dst_dir)/ipa/header-logo.png: $(BRANDS)/Sources/centos-ipa-whiteheader.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x20 $@
	$(PNGQUANT) $@

$(dst_dir)/ipa/product-name.png: $(BRANDS)/Sources/centos-ipa-whiteheader.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x20 $@
	$(PNGQUANT) $@

$(dst_dir)/ipa/login-screen-logo.png: $(BRANDS)/Sources/centos-gdm-whitelogo.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x48 $@
	$(PNGQUANT) $@

#===============================================================================
# Icons
#===============================================================================
$(dst_dir)/icons/hicolor/scalable/apps/start-here.svg: $(BRANDS)/Sources/centos-symbol.svg
	$(INKSCAPE) --export-plain-svg --export-filename=$@ $<

$(dst_dir)/icons/hicolor/scalable/apps/fedora-logo-icon.svg: $(BRANDS)/Sources/centos-symbol.svg
	$(INKSCAPE) --export-plain-svg --export-filename=$@ $<

$(dst_dir)/icons/hicolor/scalable/apps/xfce4_xicon1.svg: $(BRANDS)/Sources/centos-symbol.svg
	$(INKSCAPE) --export-plain-svg --export-filename=$@ $<

$(dst_dir)/icons/hicolor/scalable/apps/org.fedoraproject.AnacondaInstaller.svg: $(BRANDS)/Sources/anaconda.svg
	$(INKSCAPE) --export-plain-svg --export-filename=$@ $<

$(dst_dir)/icons/hicolor/symbolic/apps/org.fedoraproject.AnacondaInstaller-symbolic.svg: $(BRANDS)/Sources/anaconda-symbolic.svg
	$(INKSCAPE) --export-plain-svg --export-filename=$@ $<

$(dst_dir)/icons/hicolor/16x16/apps/fedora-logo-icon.png: $(BRANDS)/Sources/centos-symbol.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x16 $@
	$(PNGQUANT) $@

$(dst_dir)/icons/hicolor/16x16/apps/system-logo-icon.png: $(BRANDS)/Sources/centos-symbol.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x16 $@
	$(PNGQUANT) $@

$(dst_dir)/icons/hicolor/22x22/apps/fedora-logo-icon.png: $(BRANDS)/Sources/centos-symbol.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x22 $@
	$(PNGQUANT) $@

$(dst_dir)/icons/hicolor/22x22/apps/system-logo-icon.png: $(BRANDS)/Sources/centos-symbol.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x22 $@
	$(PNGQUANT) $@

$(dst_dir)/icons/hicolor/24x24/apps/fedora-logo-icon.png: $(BRANDS)/Sources/centos-symbol.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x24 $@
	$(PNGQUANT) $@

$(dst_dir)/icons/hicolor/24x24/apps/system-logo-icon.png: $(BRANDS)/Sources/centos-symbol.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x24 $@
	$(PNGQUANT) $@

$(dst_dir)/icons/hicolor/32x32/apps/fedora-logo-icon.png: $(BRANDS)/Sources/centos-symbol.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x32 $@
	$(PNGQUANT) $@

$(dst_dir)/icons/hicolor/32x32/apps/system-logo-icon.png: $(BRANDS)/Sources/centos-symbol.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x32 $@
	$(PNGQUANT) $@

$(dst_dir)/icons/hicolor/36x36/apps/fedora-logo-icon.png: $(BRANDS)/Sources/centos-symbol.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x36 $@
	$(PNGQUANT) $@

$(dst_dir)/icons/hicolor/36x36/apps/system-logo-icon.png: $(BRANDS)/Sources/centos-symbol.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x36 $@
	$(PNGQUANT) $@

$(dst_dir)/icons/hicolor/48x48/apps/fedora-logo-icon.png: $(BRANDS)/Sources/centos-symbol.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x48 $@
	$(PNGQUANT) $@

$(dst_dir)/icons/hicolor/48x48/apps/system-logo-icon.png: $(BRANDS)/Sources/centos-symbol.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x48 $@
	$(PNGQUANT) $@

$(dst_dir)/icons/hicolor/48x48/apps/anaconda.png: $(BRANDS)/Sources/anaconda.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) +dither -resize x48 $@
	$(PNGQUANT) $@

$(dst_dir)/icons/hicolor/96x96/apps/fedora-logo-icon.png: $(BRANDS)/Sources/centos-symbol.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x96 $@
	$(PNGQUANT) $@

$(dst_dir)/icons/hicolor/96x96/apps/system-logo-icon.png: $(BRANDS)/Sources/centos-symbol.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x96 $@
	$(PNGQUANT) $@

$(dst_dir)/icons/hicolor/256x256/apps/fedora-logo-icon.png: $(BRANDS)/Sources/centos-symbol.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x256 $@
	$(PNGQUANT) $@

$(dst_dir)/icons/hicolor/256x256/apps/system-logo-icon.png: $(BRANDS)/Sources/centos-symbol.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x256 $@
	$(PNGQUANT) $@

#===============================================================================
# Pixmaps
#===============================================================================
$(dst_dir)/pixmaps/fedora-logo-sprite.svg: $(BRANDS)/Sources/centos-symbol.svg
	$(INKSCAPE) --export-height=252 --export-plain-svg --vacuum-defs --export-filename=$@ $<

$(dst_dir)/pixmaps/fedora-logo-sprite.png: $(BRANDS)/Sources/centos-symbol.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x252 $@
	$(PNGQUANT) $@

$(dst_dir)/pixmaps/fedora-gdm-logo.png: $(BRANDS)/Sources/centos-gdm-whitelogo.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x48 $@
	$(PNGQUANT) $@

$(dst_dir)/pixmaps/fedora-logo.png: $(BRANDS)/Sources/centos-logo.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x80 $@
	$(PNGQUANT) $@

$(dst_dir)/pixmaps/fedora-logo-small.png: $(BRANDS)/Sources/centos-logo.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x48 $@
	$(PNGQUANT) $@

$(dst_dir)/pixmaps/fedora-logo-med.png: $(BRANDS)/Sources/centos-logo.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x64 $@
	$(PNGQUANT) $@

$(dst_dir)/pixmaps/poweredby.png: $(BRANDS)/Sources/centos-poweredby-logo.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) +dither -resize x80 $@
	$(PNGQUANT) $@

$(dst_dir)/pixmaps/system-logo-white.png: $(BRANDS)/Sources/centos-gdm-whitelogo.svg
	$(INKSCAPE) --export-height=512 --export-filename=$@ $<
	$(MOGRIFY) $(MOGRIFY_ARGS) -resize x80 $@
	$(PNGQUANT) $@

#===============================================================================
# fedora
#===============================================================================
$(dst_dir)/fedora/fedora_logo.svg: $(BRANDS)/Sources/centos-logo.svg
	$(INKSCAPE) --export-plain-svg --export-filename=$@ $<

$(dst_dir)/fedora/fedora_logo_lightbackground.svg: $(BRANDS)/Sources/centos-logo.svg
	$(INKSCAPE) --export-plain-svg --export-filename=$@ $<

$(dst_dir)/fedora/fedora_logo_darkbackground.svg: $(BRANDS)/Sources/centos-whitelogo.svg
	$(INKSCAPE) --export-plain-svg --export-filename=$@ $<

.PHONY: clean
clean:
	rm -r $(testpage-css-dir)
	rm -r $(testpage-img-dir)
	rm -r $(testpage-webfonts-dir)
	rm $(dst_files)
