## Web server test page

The web server test page is what the user sees after installing an HTTP web
servers (e.g., apache, nginx). This page exists to test the proper operation of
the HTTP server.

![Test page screenshot](testpage/screenshot.png)
